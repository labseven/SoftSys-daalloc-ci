# SoftSys-daalloc
**Matthew Beaudouin, Seungin Lyu, Adam Novotny**;
[Softsys Spring 2019](https://sites.google.com/site/softsys19/)

Implementing a dynamically sized array with a custom memory allocator.

## Resources
- [Malloc POSIX Interface](https://pubs.opengroup.org/onlinepubs/9699919799/functions/malloc.html)
- [Glib Source Code](https://code.woboq.org/userspace/glibc/malloc/malloc.c.html)
- [Sample dynamically sized array implementation](https://www.happybearsoftware.com/implementing-a-dynamic-array)
- [Custom memory allocator implementation](http://gee.cs.oswego.edu/dl/html/malloc.html)
